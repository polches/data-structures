#include <iostream>
#include <vector>
#include<list>
using namespace std;

//class Entry
template<typename K, typename V>
class Entry{
public:
	Entry(const K& k=K(), const V& v=V())
		:_key(k), _value(v) {} //we aasingn values inside constructor


	const K& key()const
	{return _key;} //return k value

	const V& value() const
	{return _value;} //return value

	void setKey(const K& k)
	{_key=k;} //set k

	void setValue(const V& v)
	{return v=_value;} //set v
private:
	K _key; //key

	V _value; };



//a c++ hash map implementation
template <typename K, typename V, typename H>

class HashMap{

public:
	typedef Entry<const K,V> Entry;

	class Iterator;

public:
	HashMap(int capacity=100)
	:n(0), B(capacity){} //constructor, we enter capacity here

	int size() const //number of entries we have
	{return n;}

	bool empty() const //is the map emty?
	{return size()==0;}

	Iterator find(const K& k); //find entry with Key k

	Iterator put(const K& k, const V& v); //insert/replace

	void erase(const K& k); //remove entry with key k

	void erase(const Iterator& p); //erase entry at p

	Iterator begin(); //iterator to first entry
	Iterator end(); //iterator to last entry



protected:

	typedef std::list<Entry> Bucket; //what is bucket? a collection of of key-value pairs
									 //list of type entry, wow
									//bucket is a list of types entry
									//rememeber: entry has a value and a key inside
	typedef std::vector<Bucket> BktArray; //this is a vector which holds type Bucket
										  //remmebr that bucket is a list of entries
										  //now we have a vector array of lists
	//insert hash map utilities here provided below

	Iterator finder(const K& k); //find utility

	Iterator inserter (const Iterator& p, const Entry& e); //insert utility

	void eraser( const Iterator& p); //remove utility

	typedef typename BktArray::iterator Bltor; //bucket iterator  (included iterator for vectors)

	typedef typename Bucket::iterator Eltor; //entry iterator

	static void nextEntry(Iterator& p) //bucket's next entry???
	{++p.enty;}

	static bool endOfBkt(const Iterator& p) //end of a bucket
	{return p.ent==p.bkt->end();}


private:

	int n; //number of entries

	H hash; //they call it the hash comparator 

	BktArray B; //we call bucket array just B

public:

	class Iterator{
	private:
	//we have two Iterator types. one for iterationg over buckets (we have several buckets)
															//we store then in an array-vector
	//another for iterating over entries of a bucket

	Eltor ent; //which entry? this is an iterator for an entry
	Bltor bkt; //which bucket? this is an iterator for a bucket

	const BktArray* ba; //which bucket array

	public:
	Iterator(const BktArray& a, const Bltor& b, const Eltor& q=Eltor())
	:ent(q), bkt(b), ba(&a) {}

	Entry& operator*() const //get entry
		{return *ent;} //return an element 

	bool operator==(const Iterator& p) const; //are iterators equal?


	Iterator& operator++();

	friend class HashMap;};

};


//function definitions for iterator here!