#include <iostream>
#include<list>
#include <stack>

using namespace std;

list<int> M;






template<typename E, typename C>
class ListPriorityQue{


public:

	//we do not include here constructor
	//bexcause we rely on a default constructor
	//inside List in STL
	int size() const; //size of priority Que

	bool empty() const; //is queue empty

	void insert(const E& e); //imsert element

	const E& min() const; //minimum element

	void removeMin(); //remove minimum

private:

	std::list<E> L; //list for a priority Que

	C isLess; //input is two values 
};


template <typename E, typename C>
int ListPriorityQue<E,C>::size() const
	{return L.size();}



template<typename E, typename C>
	bool ListPriorityQue<E,C>::empty() const
	{return L.empty();}



template<typename E, typename C>
	void ListPriorityQue<E,C>::insert(const E& e){
			//we now have a list of type E, and we 
			//want to get acces to iterator of that list


			//we need to include "typename" when we use type E here
			//not many dataild are provided about that typename
			typename std::list<E>::iterator p;
			p=L.begin();

			//we insert elements in order
			//if e is the larget element which we want to insert
			//than we insert it at the end of the list
			//we insert elements in order 
			//first one is the smallest; last one is the biggest

			while (p!=L.end() && !isLess(e,*p)){
				++p;
				L.insert(p,e); //insert e before p;
			}
	}


template<typename E, typename C>
const E& ListPriorityQue<E,C>::min() const
{ return L.front(); }


template<typename E, typename C>
void ListPriorityQue<E,C>::removeMin()
	{L.pop_front();}




